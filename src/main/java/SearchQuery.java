/** SearchQuery Class for Searching for an Specific Thingy Using Objects **/

public class SearchQuery {

    String song, artist, album;

    public SearchQuery () {
        this.song = null;
        this.artist = null;
        this.album = null;
    }

    public SearchQuery ( String song, String artist, String album) {
        this.song = song;
        this.artist = artist;
        this.album = album;
    }
}
