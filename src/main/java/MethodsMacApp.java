/** The custom methods for the Mac Spotify Application **/

import org.sikuli.script.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MethodsMacApp {

    // Testing Ground
    public static void main(String arg[]) throws Exception
    {
        bringSpotifyFront();
    }

    // Launch Spotify Native Application if Mac or Windows
    public static void launchSpotifyApp () throws IOException{

        // Declare which Operating System User is On
        String os = System.getProperty("os.name","generic").toLowerCase();
        // If Else Statement to Launch Windows/Mac Spotify Native App
        // Macintosh
        if (os.contains("mac"))
        {
            Runtime.getRuntime().exec("open -a Spotify");
            // System.out.println("Its A Macintosh");
        }
        // Windows *
        else if (os.contains("windows"))
        {
            Runtime.getRuntime().exec("Spotify.exe");
            // System.out.println("Its A Windows");
        }
    }


    // Verify User Is Logged In (Unless Assumed User is Logged In)
    // Image Method - If Not Logged In, Launches + Signs In
    public static void verifyUserLogged() throws Exception
    {
        // Declare the Properties FIle
        Properties prop = new Properties();
        InputStream input = null;
        // Load the properties file
        input = new FileInputStream("src/main/java/configmacapp.properties");
        prop.load(input);

        Screen s = new Screen();
        if (verifyImageExist(prop.getProperty("searchbar"))) // checks if constant searchbar exists
        {
            System.out.println("The User Is Logged In");
        }
        else
        {
            System.out.println("The User Is Not Logged In");
            Thread.sleep(5000);
            Match region = s.find(prop.getProperty("rememberme"));
            // Username
            region.setY(region.above(86).getY());
            region.doubleClick();
            region.type("a",KeyModifier.CMD);
            System.out.println("Cleared Out Username Query");
            region.type(prop.getProperty("user_cred"));
            // Password
            region.setY(region.below(43).getY());
            region.click();
            region.type("a", KeyModifier.CMD);
            System.out.println("Cleared Out Password Query");
            region.type(prop.getProperty("user_pw_pass"));
            // Click Login Button
            s.click(prop.getProperty("login_button"));
            verifyUserLogged();
        }
    }

    // Verify if Image Exists
    public static boolean verifyImageExist(String path) {

        Screen s = new Screen();
        try{
            s.wait(path);
            return true;
        }
        catch(FindFailed e) {
            return false;
        }

    }

    // Navigate Dropdown Top Right Navbar (For future testing of other options)
    public static void navigateDropdownNav(String element) throws Exception
    {
        // verifyViewAccount();
        // Declare the Properties FIle
        Properties prop = new Properties();
        InputStream input = null;
        // Load the properties file
        input = new FileInputStream("src/main/java/configmacapp.properties");
        prop.load(input);
        // Create the Screen
        Screen s = new Screen();
        s.click();

        // Navigate to Dropdown Navbar
        s.wait(prop.getProperty("arrow_dropdown"));
        s.click(prop.get("arrow_dropdown"));

        // Different options
        if(element.contains("privatesession")) // private session
        {
            s.wait(prop.getProperty("privatesession_dropdown"));
            s.click(prop.getProperty("privatesession_dropdown"));
        }
        else if (element.contains("settings")) // settings page
        {
            s.wait(prop.getProperty("settings_dropdown"));
            s.click(prop.getProperty("settings_dropdown"));
        }
        else if (element.contains("account")) // settings page
        {
            s.wait(prop.getProperty("settings_dropdown"));
            s.click(prop.getProperty("settings_dropdown"));
        }
        else if (element.contains("upgrade")) // upgrade account
        {
            s.wait(prop.getProperty("upgrade_dropdown"));
            s.click(prop.getProperty("upgrade_dropdown"));
        }
        else if (element.contains("log")) // logout
        {
            s.wait(prop.getProperty("logout_dropdown"));
            s.click(prop.getProperty("logout_dropdown"));
        }
    }

    // Verifies the User is Online and Logs Them Out o.O
    public static void logoutUser() throws Exception {
        // Declare the Properties FIle
        Properties prop = new Properties();
        InputStream input = null;
        // Load the properties file
        input = new FileInputStream("src/main/java/configmacapp.properties");
        prop.load(input);

        Screen s = new Screen();
        if(verifyImageExist(prop.getProperty("leftnavbar")))
        {
            navigateDropdownNav("logout");
            System.out.println("User is Logged In...Proceeding to Logout");
            Thread.sleep(2000);
        }
        else
        {
            System.out.println("User is already Logged Out Continue On...");
        }
    }


    // Bring Target Application to Active (Optional - Only if not using Multiple Monitors)
    // For this instance, I assume that I have multiple monitors because I personally do
    // Only for Mac on Task Bar, Cannot do it on PURE Java Code so its a hack/workaround using Sikuli (Non-Selenium)
    public static void bringSpotifyFront() throws Exception {

        // Declare the Properties FIle
        Properties prop = new Properties();
        InputStream input = null;
        // Load the properties file
        input = new FileInputStream("src/main/java/configmacapp.properties");
        prop.load(input);
        // Create the Screen
        Screen s = new Screen();

        while(!(verifyImageExist(prop.getProperty("spotifybrand")) | verifyImageExist(prop.getProperty("playcontrols"))))
        {
            launchSpotifyApp();
        }
    }
}




