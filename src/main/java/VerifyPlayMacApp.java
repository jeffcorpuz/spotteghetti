/** Verify the Play Music Button Track **/

import org.sikuli.script.Match;
import org.sikuli.script.Screen;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class VerifyPlayMacApp extends MethodsMacApp {

    // Testing Ground
    public static void main(String arg[]) throws Exception {

        bringSpotifyFront();
        verifyAudioPlay(10);
    }

    // Verify if the Audio Is Playing (Image Method - Native Sikuli)
    public static void verifyAudioPlay(int seconds) throws Exception {

        // Check if User Is Logged In (Optional) Assume User is Logged In
        // verifyUserLogged();

        // Declare the Properties FIle
        Properties prop = new Properties();
        InputStream input = null;
        // Load the properties file
        input = new FileInputStream("src/main/java/configmacapp.properties");
        prop.load(input);
        // Create the Screen
        Screen s = new Screen();
        // bringSpotifyFront(); s.click(); // Only used for Single Monitor Situation
        // verifyUserLogged(); Verify User is Logged In (Optional, Assumed User is Logged In)

        Match region = s.find(prop.getProperty("playcontrols"));
        region.setW(1440); // Extend to the Right
        // Click the Play Button Icon
        s.click(prop.getProperty("playbutton"));
        // Wait a Set Interval
        if((region.waitVanish(prop.getProperty("defaultzero"), seconds * 1000 ) | region.waitVanish(prop.getProperty("playbutton"), seconds * 1000 )))
        {
            System.out.println("Successful Play");
        }
        else
        {
            System.out.println("Player is not playing :(");
        }

    }

}
