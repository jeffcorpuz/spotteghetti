/** Verify the Search Functionality (Image Method) & (OCR Method - OPTIONAL) **/

import org.sikuli.script.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;


public class VerifySearchMacApp extends MethodsMacApp {

    // Testing Ground
    public static void main (String arg[]) throws Exception {

        bringSpotifyFront();
        String[] testarray = new String[] {"the cat empire", "the beatles","pink floyd","funkadelic","ajsdfiajsdfisadfjasdf"};
        // verifySearchItem("anything you want");
        verifySearchArray(testarray);
    }

    // Verify One Item of Your Own Choosing
    public static void verifySearchItem(String element) throws Exception
    {
        // Declare the Properties FIle
        Properties prop = new Properties();
        InputStream input = null;
        // Load the properties file
        input = new FileInputStream("src/main/java/configmacapp.properties");
        prop.load(input);

        Screen s = new Screen();
        // Verify User is Logged In (Optional, Assumed User is Logged In)
        verifyUserLogged();

        // Go to Search Bar and Clear if the Bar is not cleared
        if (!verifyImageExist(prop.getProperty("searchdefault")))
        {
            System.out.println("There is an Existing Query, Clearing Search Box");
            s.click(prop.getProperty("clearsearch"));
        }
        else
        {
            System.out.println("The Search Query Box is Cleared, Continue On");
        }
        // Type Search Stuff
        s.wait(prop.getProperty("searchbar"), 0);
        s.click(prop.getProperty("searchbar"),0);
        // Enter Parameter in Search Bar
        s.type(element);

        // Wait for All Results Bar Under Seaarch Bar then Click
        s.wait(prop.getProperty("showingresults_bar"));
        s.click(prop.getProperty("showingresults_bar"));
        // Verify if in Results Page
        if (verifyImageExist(prop.getProperty("validresults_page"))) // Valid Results
        {
            System.out.println("The String returns a valid results page");
        }
        else if (verifyImageExist(prop.getProperty("falseresults_page"))) // False Results Page
        {
            System.out.println("The String returns a non-valid results page");
        }
        else // Browser Crashed
        {
            System.out.println("No Page is Found");
        }

        System.out.println("Search Test for " + element +" Completed");
    }

    // Verify Search Functionality with an Array of Items
    public static void verifySearchArray(String[] items) throws Exception {

        // Declare the Properties FIle
        Properties prop = new Properties();
        InputStream input = null;
        // Load the properties file
        input = new FileInputStream("src/main/java/configmacapp.properties");
        prop.load(input);

        Screen s = new Screen();
        // bringSpotifyFront();
        s.click(); // Only used for Single Monitor Situation
        // Verify User is Logged In (Optional, Assumed User is Logged In)
        verifyUserLogged();

        // Go through the items
        for(int i = 0; i < items.length; i++)
        {
            // verifySearchItem(items[i]); // Another Method Just Recalling the Array Back and Forth ETC

            // Go to Search Bar and Clear if the Bar is not cleared
            if (!verifyImageExist(prop.getProperty("searchdefault")))
            {
                System.out.println("There is an Existing Query, Clearing Search Box");
                s.click(prop.getProperty("clearsearch"));
            }
            else
            {
                System.out.println("The Search Query Box is Cleared, Continue On");
            }
            // Type Search Stuff
            s.wait(prop.getProperty("searchbar"), 0);
            s.click(prop.getProperty("searchbar"),0);
            // Enter Parameter in Search Bar
            s.type(items[i]);

            // Wait for All Results Bar Under Seaarch Bar then Click
            s.wait(prop.getProperty("showingresults_bar"));
            s.click(prop.getProperty("showingresults_bar"));
            // Verify if in Results Page
            if (verifyImageExist(prop.getProperty("validresults_page"))) // Valid Results
            {
                System.out.println("The String returns a valid results page");
            }
            else if(verifyImageExist(prop.getProperty("falseresults_page"))) // False Results Page
            {
                System.out.println("The String returns a non-valid results page");
            }

            System.out.println("Search Test for " + items[i] + " Completed");

            // Clear Search Bar for Other Items
            s.wait(prop.getProperty("clearsearch"));
            s.click(prop.getProperty("clearsearch"));
        }

        System.out.println("Search Test (Array) Completed");
    }

    // Scenario Look Up Artist/Song/Album Using SearchQuery Class IF Defined Already xD (Optional) non-native Sikuli
    // This uses OCR (Tesseract), therefore make sure system is configured correctly
    // One can also use JSON stuff
    public static void verifySearchUsingQuery(SearchQuery element) throws Exception {

        // Declare the Properties FIle
        Properties prop = new Properties();
        InputStream input = null;
        // Load the properties file
        input = new FileInputStream("src/main/java/configmacapp.properties");
        prop.load(input);

        Screen s = new Screen();
        s.click();
        // Verify User is Logged In (Optional, Assumed User is Logged In)
        // verifyUserLogged();

        // Declare Containers for OCR Text
        String ocrSong,ocrArtist,ocrAlbum;

        // Go to Search Bar
        s.wait(prop.getProperty("searchbar"), 0);
        s.click(prop.getProperty("searchbar"),0);
        // Enter Parameter in Search Bar
        s.type(element.song);
        // Wait for All Results Bar Under Seaarch Bar then Click
        s.wait(prop.getProperty("showingresults_bar"));
        s.click(prop.getProperty("showingresults_bar"));
        // One can skip this and go to direct song/artist/album if found in search dropdown...
        // To be added eventually since this is the optional one
        // Locate the Song in the Query Page
        s.findText("SONG");
        s.findText(element.song);
        // Locate the Artist in the Query Page
        s.findText("ARTIST");
        s.findText(element.artist);
        // Locate the Album in the Query Page
        s.findText("ALBUM");
        s.findText(element.album);
    }
}
