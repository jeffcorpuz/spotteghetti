/** Verify if the User can successfully open the setttings page and check the items **/

import org.sikuli.script.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class VerifySettingsMacApp extends MethodsMacApp {

    public static void main(String arg[]) throws Exception {

        bringSpotifyFront();
        verifySettings(3);

    }

    // Verify the Settings Page (Image Method - Native Sikuli)
    // Can choose three modes -> simple, basic settings, or exhaustive
    public static void verifySettings(int mode) throws Exception {


        // Declare the Properties FIle
        Properties prop = new Properties();
        InputStream input = null;
        // Load the properties file
        input = new FileInputStream("src/main/java/configmacapp.properties");
        prop.load(input);
        // Create the Screen
        Screen s = new Screen();
        // bringSpotifyFront(); s.click(); // Only used for Single Monitor Situation
        // verifyUserLogged(); Verify User is Logged In (Optional, Assumed User is Logged In)

        // Declare the Paths for Images for Simple Settings - CONSTANT
        String[] simpleSettings = new String[] {prop.getProperty("language_settings"),
                prop.getProperty("musicquality_settings"),prop.getProperty("social_settings"),
                prop.getProperty("localfiles_settings"),prop.getProperty("display_settings")};

        // Declare the Paths for Images for Advanced Settings - CONSTANT
        String[] advancedSettings = new String[] {prop.getProperty("playback_settings"),
                prop.getProperty("startup_settings"),prop.getProperty("cache_settings"),
                prop.getProperty("proxy_settings")};

        // Declare the Paths for BOTH Simple + Advanced Settings - CONSTANT (Optional)
        String[] exhaustiveSettings = new String[simpleSettings.length + advancedSettings.length];
        // Array Copy+Pasterino // Too Lazy to Declare them As an ArrayList
        System.arraycopy(simpleSettings,0,exhaustiveSettings,0,simpleSettings.length);
        System.arraycopy(advancedSettings,0,exhaustiveSettings,simpleSettings.length,advancedSettings.length);

        // Navigate to Settings Page
        // Objective Way (Optional, Preferred Method)
        navigateDropdownNav("settings");

        s.click();

        // Navigate to the Top of the Settings Page
        while(!verifyImageExist(prop.getProperty("viewaccount_settings")))
        {
            s.wheel(2,4);
        }

        // Literal Way if not using Objective Way :o
        // s.click();
        // s.wait(prop.getProperty("arrow_dropdown"));
        // s.click(prop.get("arrow_dropdown"));
        // s.wait(prop.getProperty("settings_dropdown"));
        // s.click(prop.getProperty("settings_dropdown"));

        // Case Switch Scenarios
        // 0 - default -    checks if the user is in the settings page
        // 1 - simple -     checks the default settings are there
        // 2 - advanced -   checks the advanced settings are there
        // 3 - exhaustive - case 1 + case 2 at the same time (Optional)
        switch (mode) {
            case 0:
                if (verifyImageExist(prop.getProperty("settingscenterfree")) || verifyImageExist(prop.getProperty("settingscenterpremium")))
                {
                    System.out.println("The User is in the Settings Page (Default)");
                }
                else
                {
                    System.out.println("User is not in the settings page");
                }
                break;
            case 1:
                for(int i = 0; i < simpleSettings.length; i++)
                {
                    // Press Down Until The Image Exists
                    while(!verifyImageExist(simpleSettings[i]))
                    {
                        s.wheel(-2,2);
                    }
                    if(verifyImageExist(simpleSettings[i]))
                    {
                        System.out.println(simpleSettings[i] + " exists, continue on");
                    }
                    else
                    {
                        System.out.println("Image Not Found, Not In Settings");
                        break;
                    }
                }

                System.out.println("All Simple Testings Have Been Verified");
                break;
            case 2:
                // Scroll down until "Show Advanced Settings Pill Button Is Shown"
                while(!verifyImageExist(prop.getProperty("showadvanced_settings")))
                {
                    s.wheel(-2,3);
                }
                // Click Advanced Settings
                s.click(prop.getProperty("showadvanced_settings"));

                for(int i = 0; i < advancedSettings.length; i++)
                {
                    // Press Down Until The Image Exists
                    while(!verifyImageExist(advancedSettings[i]))
                    {
                        s.wheel(-2,3);
                    }
                    if(verifyImageExist(advancedSettings[i]))
                    {
                        System.out.println(advancedSettings[i] + " exists, continue on");
                    }
                    else
                    {
                        System.out.println("Image Not Found, Not In Settings");
                        break;
                    }
                }
                while(!verifyImageExist(prop.getProperty("hideadvanced_settings")))
                {
                    s.wheel(-2,3);
                }
                // Click Hide Advanced Settings
                s.click(prop.getProperty("hideadvanced_settings"));

                System.out.println("All Advanced Testings Have Been Verified");
                break;
            case 3:
                for(int i = 0; i < exhaustiveSettings.length; i++)
                {
                    // Press Down Until The Image Exists
                    while(!verifyImageExist(exhaustiveSettings[i]))
                    {
                        s.wheel(-2,2);
                    }
                    if(verifyImageExist(exhaustiveSettings[i]))
                    {
                        System.out.println(exhaustiveSettings[i] + " exists, continue on");
                        // Trigger the Advanced Settings
                        if(i == 4)
                        {
                            System.out.println("Clicking Advanced Settings");
                            while(!verifyImageExist(prop.getProperty("showadvanced_settings")))
                            {
                                s.wheel(-2,3);
                            }
                            s.click(prop.getProperty("showadvanced_settings"));
                        }
                    }
                    else
                    {
                        System.out.println("Image Not Found, Not In Settings");
                        break;
                    }
                }
                System.out.println("All Settings Have Been Tested For");
                break;
        }

    }

    // Verify the View Account (Image Method - Native Sikuli) (Optional) Just to see if the Navbar Method works with other parameters
    // User clicks "View Account" + Successful Launch of Browser with Proper URL
    public static void verifyViewAccount() throws Exception {

        // Declare the Properties FIle
        Properties prop = new Properties();
        InputStream input = null;
        // Load the properties file
        input = new FileInputStream("src/main/java/configmacapp.properties");
        prop.load(input);
        // Create the Screen
        Screen s = new Screen();
        // bringSpotifyFront(); s.click(); // Only used for Single Monitor Situation
        // verifyUserLogged(); Verify User is Logged In (Optional, Assumed User is Logged In)

        // Click the Dropdown Menu and Select "Account"
        // Navigate to Settings Page
        // Objective Way (Optional, Preferred Method)
        navigateDropdownNav("Account");
    }

}
