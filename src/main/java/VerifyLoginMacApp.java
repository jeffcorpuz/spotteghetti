/** Verify the Login Process for Spotify Mac App Using Regions + Images **/

import org.sikuli.script.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class VerifyLoginMacApp extends MethodsMacApp {

    // Testing Ground
    public static void main(String arg[]) throws Exception {

        bringSpotifyFront();
        String[] testParam = new String[] {"pass","fail"};
        for(int i=0; i <testParam.length; i++)
        {
            verifyLogInProcess(testParam[i]);
        }
    }

    // Verify Log-In Process, Either Pass or Failure, or Both (Defaulted)
    // Image Method NOT Region Method
    public static void verifyLogInProcess(String mode) throws Exception {

        // Declare the Properties FIle
        Properties prop = new Properties();
        InputStream input = null;
        // Load the properties file
        input = new FileInputStream("src/main/java/configmacapp.properties");
        prop.load(input);

        // Checks if the User is Logged In...and Signs them Out :P
        logoutUser();

        // bringSpotifyFront(); s.click(); // Only used for Single Monitor Situation
        Screen s = new Screen();
        // Find the Region of the Remember Me Box
        Match region = s.find(prop.getProperty("rememberme"));

        // if else statements for pass/failure or both (defaulted)
        // Passing Method
        if(mode.contains("pass"))
        {

            // Username
            region.setY(region.above(86).getY());
            region.doubleClick();
            region.type("a",KeyModifier.CMD); // Clears Out
            System.out.println("Cleared Out Username Query");
            region.type(prop.getProperty("user_cred"));
            // Password
            region.setY(region.below(43).getY());
            region.click();
            region.type("a", KeyModifier.CMD); // Clears Out
            System.out.println("Cleared Out Password Query");
            region.type(prop.getProperty("user_pw_pass"));

            // Click Login Button
            s.click(prop.getProperty("login_button"));

            Thread.sleep(2000);
            if(verifyImageExist(prop.getProperty("leftnavbar")))
            {
                System.out.println("Succesful Login");
            }
            else
            {
                System.out.println("Non-Successful Login");
            }
        }
        // Failing Method
        else if(mode.contains("fail"))
        {
            // Username
            region.setY(region.above(86).getY());
            region.doubleClick();
            region.type("a",KeyModifier.CMD);
            System.out.println("Cleared Out Username Query");
            region.type(prop.getProperty("user_cred"));
            // Password
            region.setY(region.below(43).getY());
            region.click();
            region.type("a", KeyModifier.CMD);
            System.out.println("Cleared Out Password Query");
            region.type(prop.getProperty("user_pw_fail"));

            // Click Login Button
            s.click(prop.getProperty("login_button"));

            // Verify if Failure Login Appears
            Thread.sleep(2000);
            if(verifyImageExist(prop.getProperty("failure")))
            {
                System.out.println("Successful Failure Screen Showed Up");
            }
            else
            {
                System.out.println("Failure Splash Did Not Show Up");
            }
        }
    }
}
