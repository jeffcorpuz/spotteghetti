# Spotteghetti

Automation stuff for the Spotify Mac Client using Maven + Sikuli + Properties File  
Majority of documentation is in the code itself 
 
------------

# How to Run Tests

Inside each 'Verify****MacApp' class  
lies a main method with the comment // testing ground  

All you have to do is press run to see what it does in main  
It is self explanatory, you call the method of whatever is in that specific class to execute  

By all means you can play around and create scenarios

------------

# Assumptions

* Connected to the Internet
* Spotify Backend Working (Always On)
* OCR (Tess) is not used since only Native Sikuli (Stable) is used
* JUnit/TestNG is not present due to not explicit need in assignment
* Uses console log + system.out.println() for status/data

--------------

## Methods

Universal methods are here  

### launchSpotifyApp
launches the spotify native application (either mac or windows)

### verifyUserLogged
checks if the user is logged (i assume the user is logged)

### verifyImageExist
got rid of the try/catch stuff to get simple Boolean result if an image exist

### navigateDropdownNav 
navigates to dropdown nav bar in the top right

### logoutUser
logs out the user

### bringSpotifyFront
custom way to bring spotify to active window IF NOT using multpile monitors (i assume i am but i put this just in case)

------------

## Verify Login

* Uses Region class in conjunction with Screen 
* Clears parameters if Username/Password filled

### verifyLogInProcess
Verifies if the user can log in successfully or not  

------------

## Verify Search

* Uses Screen class (Image Method)
* Object Oriented Query is optional

### verifySearchItem
takes a string input to look up said query 

### verifySearchArray
takes string inputs in a given array for search queries

### verifySearchUsingQuery (Optional)
takes an object that takes in either song,artist, and/or album

-------------


## Verify Play

* Uses Screen class + Region Class

### verifyAudioPlay
Uses region to waitVanish until the 0:00 time stamp to validate playing

-------------

## Verify Settings (Custom)

### verifySettings
three different modes are able to be chosen   
this verifies if the user is in the settings as well see if the default/advanced are present

